La practica se a hecho exactamente igual que lo que pide el enunciado y los cases del program estan repartidos de la misma forma que
el enunciado.

La disposicion de archivos y carpetas es la siguiente:

En la carpeta principal nos encontraremos todos los archivos de claves, resumen y comandas que se piden en el enunciado. 
En caso de guardar en el ejercicio 11 y 12 una clave se guardara aqui.

Dentro de la carpeta src esta distribuido el proyecto en 4 paquetes:

Criptografia: Contiene todo el codigo para generar claves y hashes.

files: Contiene los modificadores de archivos, es decir, codigo que escribe y lee de archivos

porgrama: Por un lado tenemos el generador de comandas y por otro Programa que es el main.

tastat: Contiene todo el mapeo de de objetos.



Una vez iniciado el programa, recordamos que esta en programa Programa.java, se pueden ir probando todos los ejercicios de uno en uno
recuerdo que el 1 y el 10 sirven para generar comandas y claves, es importante hacer estos 2 antes del resto para que todo 
funcione ya que muchos ejercicios dependen de que se usen estos 2 antes.