package programa;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import Criptografia.AESKeys;
import Criptografia.Hashes;
import Criptografia.RSAKeys;
import files.BinaryFiles;
import files.Serialization;
import tastat.Client;
import tastat.Comanda;
import tastat.DiariMoviments;
import tastat.Magatzem;
import tastat.Operari;
import tastat.OrdreCompra;
import tastat.Producte;
import tastat.Proveidor;

public class Programa {
	static Scanner input;
	static RSAKeys rsaKeys;
	static AESKeys aesKeys;
	static Hashes hashes;
	static String filename;

	public static void main(String[] args) {

		// 1.- Generaci� d'un magatzem petit�
		Magatzem elMeuMagatzem = new Magatzem(new ArrayList<Producte>(), new ArrayList<Client>(),
				new ArrayList<Comanda>(), new ArrayList<Proveidor>(), new ArrayList<Operari>(),
				new ArrayList<DiariMoviments>(), new ArrayList<OrdreCompra>());

		Generadors.generarClients(elMeuMagatzem.getClients());
		Generadors.generarProveidors(elMeuMagatzem.getProveidors());
		Generadors.generarProductes(elMeuMagatzem);
		Generadors.generarComandes(elMeuMagatzem, elMeuMagatzem.getClients().get(0));
		// Generadors.veureTot(elMeuMagatzem);

		rsaKeys = new RSAKeys();
		aesKeys = new AESKeys();
		hashes = new Hashes();

		menu(elMeuMagatzem);

	}

	static void menu(Magatzem mgz) {
		input = new Scanner(System.in);
		int seleccionado = 0;
		// System.out.println("\n\n\n");
		String tipoClave;
		do {
			seleccionado = displayMenu();

			switch (seleccionado) {
			case 1:
				Generadors.generarComandes(mgz, mgz.getClients().get(0));
				break;
			case 2:
				Programa.showComandes(mgz.getComandes());
				break;
			case 3:
				Serialization.serializeIterable("comandesClar.dat", mgz.getComandes());
				break;
			case 4:
				List<Comanda> comandes = Serialization.<Comanda>deserialize("comandesClar.dat");
				mgz.getComandes().clear();
				mgz.getComandes().addAll(comandes);
				System.out.println(comandes);
				break;
			case 5:
				if (mgz.getComandes().size() == 0) {
					System.out.println("No hay comandas.");
					break;
				}
				if (!rsaKeys.hasKeys()) {
					System.out.println("No hay claves RSA.");
					break;
				}
				Serialization.serializeIterable("comandesClar.dat", mgz.getComandes());
				byte[] hash = hashes.hashOfFile("comandesClar.dat");
				hash = rsaKeys.sign(hash);
				BinaryFiles.writeBytes("resum.dat", hash);
				System.out.println("Comandas guardadas con resumen.");
				break;
			case 6:
				byte[] bytes = BinaryFiles.readBytes("resum.dat");
				bytes = rsaKeys.unsign(bytes);
				byte[] hashBytes = hashes.hashOfFile("comandesClar.dat");
				if (Arrays.equals(bytes, hashBytes)) {
					comandes = Serialization.<Comanda>deserialize("comandesClar.dat");
					mgz.getComandes().clear();
					mgz.getComandes().addAll(comandes);
					System.out.println("Archivo cargado correctamente.");
				} else {
					System.out.println("El archivo ha sido modificado.");
				}
				break;
			case 7:
				if (mgz.getComandes().size() == 0) {
					System.out.println("No hay comandas.");
					break;
				}
				if (rsaKeys.getPublicKey() == null) {
					System.out.println("No hay clave publica.");
					break;
				}
				aesKeys.generateKey();
				bytes = BinaryFiles.readBytes("comandesClar.dat");
				bytes = aesKeys.cipher(bytes);
				BinaryFiles.writeBytes("comandesAes.dat", bytes);
				aesKeys.saveEncriptedKey("aes.key", rsaKeys);

				break;
			case 8:
				bytes = BinaryFiles.readBytes("aes.key");
				bytes = rsaKeys.decrypt(bytes);
				aesKeys.generateFromBytes(bytes);
				byte[] fileBytes = BinaryFiles.readBytes("comandesAes.dat");
				
				System.out.println("longitud " + bytes.length);
				fileBytes = aesKeys.decipher(fileBytes);

				ByteArrayInputStream in = new ByteArrayInputStream(fileBytes);
				ObjectInputStream is = null;
				try {
					is = new ObjectInputStream(in);
					mgz.getComandes().clear();
					while (true) {
						mgz.getComandes().add((Comanda) is.readObject());
					}
				} catch (IOException e) {
					//e.printStackTrace();
					try {
						is.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
				System.out.println("Archivo cargado.");
				break;
			case 10:
				rsaKeys.generateKeys();
				System.out.println("Claves generadas.");
				break;
			case 11:
				System.out.print("Nombre del archivo donde guardar: ");
				filename = input.nextLine();
				rsaKeys.savePublicKey(filename);
				break;
			case 12:
				System.out.print("Nombre del archivo donde guardar: ");
				filename = input.nextLine();
				rsaKeys.savePrivateKey(filename);
				break;
			case 13:
				System.out.print("¿Que clave quieres recuperar? (privada | publica): ");
				tipoClave = input.nextLine();
				System.out.print("Nombre del archivo: ");
				filename = input.nextLine();
				if ("privada".equals(tipoClave)) {
					rsaKeys.loadPrivateKey(filename);
					System.out.println("Clave privada cargada.");
				} else if ("publica".equals(tipoClave)) {
					rsaKeys.loadPublicKey(filename);
					System.out.println("Clave publica cargada.");
				} else {
					System.out.println("Opción incorrecta.");
				}
				break;
			case 14:
				System.out.print("¿Que clave quieres ver? (privada | publica): ");
				tipoClave = input.nextLine();
				System.out.print("Nombre del archivo: ");
				filename = input.nextLine();
				if ("privada".equals(tipoClave)) {
					System.out.print("Clave privada: ");
					rsaKeys.readPrivateKey(filename);
				} else if ("publica".equals(tipoClave)) {
					System.out.print("Clave publica: ");
					rsaKeys.readPublicKey(filename);
				} else {
					System.out.println("Opción incorrecta.");
				}
				break;
			case 15:
				aesKeys.generateKey();
				System.out.println("Clave generada.");
				break;
			case 16:
				System.out.print("Nombre del archivo donde guardar: ");
				filename = input.nextLine();
				aesKeys.saveKey(filename);
				break;
			case 0:
				System.out.println("Bye");
				break;
			default:
				break;
			}
		} while (seleccionado != 0);
	}

	public static int displayMenu() {
		System.out.println("1.- Generar Comandes");
		System.out.println("2.- Veure Comandes");
		System.out.println("3.- Generar Fitxer de Comandes per enviar en Clar");
		System.out.println("4.- Rebre Fitxer de Comandes en Clar");
		System.out.println("5.- Generar Fitxer de Comandes per enviar Signat");
		System.out.println("6.- Rebre Fitxer de Comandes Signat");
		System.out.println("7.- Generar Fitxer de Comandes Xifrat amb clau");
		System.out.println("8.- Rebre Fitxer de Comandes Xifrat");

		System.out.println("\n10.- Generar parell de Claus");
		System.out.println("11.- Gravar clau p�blica en fitxer");
		System.out.println("12.- Gravar clau privada en fitxer");
		System.out.println("13.- Recuperar clau de Fitxer");
		System.out.println("14.- Veure claus del Fitxer");
		System.out.println("\n\n0.- Sortir");
		System.out.print("Opci�n: ");

		int seleccionado = -1;
		try {
			seleccionado = input.nextInt();
		} catch (Exception e) {

		}
		input.nextLine();
		return seleccionado;
	}

	private static void showComandes(List<Comanda> comandas) {
		for (Comanda c : comandas) {
			System.out.println(c);
		}
	}
}
