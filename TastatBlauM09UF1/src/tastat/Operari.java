package tastat;

import java.io.Serializable;
import java.util.Set;

public class Operari  implements Serializable {

	protected int idOperari;
	protected String nomOperari;
	protected int numPastissos;
	protected Magatzem mgz;

	Operari() {
		idOperari = Generador.getNextOperari();
		Thread.currentThread().setName(Integer.toString(idOperari));
		numPastissos = 0;
	}

	Operari(Magatzem mgz) {
		this();
		this.mgz = mgz;
	}

	public int getNumPastissos() {
		return numPastissos;
	}

	public void setNumPastissos(int numPastissos) {
		this.numPastissos = numPastissos;
	}

	Operari(String nom) {
		this();
		nomOperari = nom;
	}

	@Override
	public String toString() {
		return (idOperari + ": " + getNomOperari());
	}

	public int getIdOperari() {
		return idOperari;
	}

	public String getNomOperari() {
		return nomOperari;
	}

	public void setNomOperari(String nomOperari) {
		this.nomOperari = nomOperari;
	}

}