package Criptografia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class RSAKeys {
	private PublicKey publicKey;
	private PrivateKey privateKey;

	public PublicKey getPublicKey() {
		return publicKey;
	}

	public PrivateKey getPrivateKey() {
		return privateKey;
	}
	
	public boolean hasKeys() {
		return publicKey != null && privateKey != null;
	}

	public void generateKeys() {
		KeyPairGenerator keyPairGenerator;
		try {
			keyPairGenerator = KeyPairGenerator.getInstance("RSA");
			KeyPair keyPair = keyPairGenerator.generateKeyPair();
			this.publicKey = keyPair.getPublic();
			this.privateKey = keyPair.getPrivate();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void savePublicKey(String filename) {
		saveKey(this.publicKey, filename);
	}

	public void savePrivateKey(String filename) {
		saveKey(this.privateKey, filename);
	}

	private void saveKey(Key key, String filename) {
		byte[] publicKeyBytes = key.getEncoded();

		FileOutputStream fos;
		try {
			fos = new FileOutputStream(filename);
			fos.write(publicKeyBytes);
			fos.flush();
			fos.close();
			System.out.println("Clave guardada.");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("No se ha guardado la clave.");
		}
	}

	public void loadPrivateKey(String filename){
		try {
		byte[] bytes = this.loadKeyBytes(filename);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		KeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
		PrivateKey keyFromBytes = keyFactory.generatePrivate(keySpec);
		this.privateKey =  keyFromBytes;
		} catch (Exception e) {
			
		}
	}
	
	public void loadPublicKey(String filename){
		try {
		byte[] bytes = this.loadKeyBytes(filename);
	    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
	    KeySpec keySpec = new X509EncodedKeySpec(bytes);
	    PublicKey keyFromBytes = keyFactory.generatePublic(keySpec);
	    this.publicKey = keyFromBytes;
		} catch (Exception e) {
			
		}
	}
	
	private byte[] loadKeyBytes(String filename) {
	      FileInputStream fis;
		try {
			fis = new FileInputStream(filename);
		      int numBtyes = fis.available();
		      byte[] bytes = new byte[numBtyes];
		      fis.read(bytes);
		      fis.close();
		      return bytes;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void readPublicKey(String filename) {
		try {
		byte[] bytes = this.loadKeyBytes(filename);
       	for (byte b : bytes) {
        	System.out.print(Integer.toHexString(0xFF & b));
      	}
        System.out.println();
		} catch (Exception e) {
			
		}
	}

	public void readPrivateKey(String filename) {
		try {
		byte[] bytes = this.loadKeyBytes(filename);
       	for (byte b : bytes) {
        	System.out.print(Integer.toHexString(0xFF & b));
      	}
        System.out.println();
		} catch (Exception e) {
			
		}
	}
	
	public byte[] sign(byte[] data) {
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		}
		try {
			cipher.init(Cipher.ENCRYPT_MODE, this.privateKey);
			return cipher.doFinal(data);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public byte[] unsign(byte[] signed) {
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		}
		try {
			cipher.init(Cipher.DECRYPT_MODE, this.publicKey);
			return cipher.doFinal(signed);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public byte[] encrypt(byte[] data) {
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		}
		try {
			cipher.init(Cipher.ENCRYPT_MODE, this.publicKey);
			return cipher.doFinal(data);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public byte[] decrypt(byte[] data) {
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		}
		try {
			cipher.init(Cipher.DECRYPT_MODE, this.privateKey);
			return cipher.doFinal(data);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
