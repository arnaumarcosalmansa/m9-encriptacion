package Criptografia;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class AESKeys {
	private SecretKey key;

	public SecretKey getKey() {
		return this.key;
	}

	public void generateKey() {
		KeyGenerator keyGen;
		try {
			keyGen = KeyGenerator.getInstance("AES");
			SecureRandom random = new SecureRandom();
			keyGen.init(random);
			SecretKey secretKey = keyGen.generateKey();
			this.key = secretKey;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	public void saveKey(String filename) {
		byte[] bytes = this.key.getEncoded();
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(filename);
			fos.write(bytes);
			fos.flush();
			fos.close();
			System.out.println("Clave guardada.");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("No se ha guardado la clave.");
		}
	}

	public byte[] cipher(byte[] data) {
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance("AES");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		}
		try {
			cipher.init(Cipher.ENCRYPT_MODE, this.key);
			return cipher.doFinal(data);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public byte[] decipher(byte[] data) {
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance("AES");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		}
		try {
			cipher.init(Cipher.DECRYPT_MODE, this.key);
			return cipher.doFinal(data);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void saveEncriptedKey(String filename, RSAKeys rsaKeys) {
		byte[] publicKeyBytes = key.getEncoded();

		publicKeyBytes = rsaKeys.encrypt(publicKeyBytes);

		FileOutputStream fos;
		try {
			fos = new FileOutputStream(filename);
			fos.write(publicKeyBytes);
			fos.flush();
			fos.close();
			System.out.println("Clave guardada.");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("No se ha guardado la clave.");
		}
	}

	public void generateFromBytes(byte[] bytes) {
		try {
		SecretKey keySpec = new SecretKeySpec(bytes, "AES");
	    this.key = keySpec;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
